
//const neataptic = require('./node_modules/neataptic/src/neataptic');
var neataptic = require('neataptic');
var fs = require('fs');

var option = 'write';


var node = neataptic.node;
var neat = neataptic.neat;
var network = neataptic.network;
var methods = neataptic.methods;
var architect = neataptic.architect;






var loadNetwork = () => {

  fs.readFile('./input.json', function (err, content) {
    if (err) throw err;
    var parseJson = JSON.parse(content);
    myNetwork = neataptic.Network.fromJSON(parseJson);
    console.log(myNetwork.activate([0, 1]));
    return myNetwork;
  });


}
//Evolving Xor

var myNetwork = loadNetwork();

async function execute() {


  var myTrainingSet = [
    { input: [0, 0], output: [0] },
    { input: [0, 1], output: [1] },
    { input: [1, 0], output: [1] },
    { input: [1, 1], output: [0] }
  ];


  if (option == 'write') {

    var results = await myNetwork.evolve(myTrainingSet, {
      mutation: methods.mutation.FFW,
      equal: true,
      cost: methods.cost.MSE,
      popsize: 100,
      elitism: 10,
      log: 10,
      error: 0.03,
      iterations: 100000,
      mutationRate: 0.5
    });



    var exported = myNetwork.toJSON();

    fs.writeFile("input.json", JSON.stringify(exported), function (err) {
      if (err) throw err;
      console.log('complete');
    });

  }

  else if (option == 'read') {


    //console.log(myNetwork);
    debugger;
  }


  console.log(myNetwork.activate([0, 0]));
  console.log(myNetwork.activate([0, 1]));
  console.log(myNetwork.activate([1, 0]));
  console.log(myNetwork.activate([1, 1]));


  //console.log(myNetwork);

  //drawGraph(myNetwork.graph(1000, 1000), '.draw');


}


async function execute2() {

  debugger;

  var network = new neataptic.Network(2, 1);



  var trainingSet = [
    { input: [0, 0], output: [0] },
    { input: [0, 1], output: [1] },
    { input: [1, 0], output: [1] },
    { input: [1, 1], output: [0] }
  ];

  /* await network.evolve(trainingSet, {
    equal: true,
    error: 0.03
  }); */

  await network.evolve(trainingSet, {
    mutation: methods.mutation.FFW,
    equal: true,
    cost: methods.cost.MSE,
    popsize: 100,
    elitism: 10,
    log: 10,
    error: 0.03,
    iterations: 100000,
    mutationRate: 0.5
  });


  console.log(myNetwork.activate([0, 0]));
  console.log(myNetwork.activate([0, 1]));
  console.log(myNetwork.activate([1, 0]));
  console.log(myNetwork.activate([1, 1]));





}

debugger;
execute2();
debugger;




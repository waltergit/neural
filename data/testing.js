var rsiCalculationEma = (candles, timeframe) => {

    //Método de Wilder
    var gain = [];
    var loss = [];
    var avgGain = [];
    var avgLoss = [];
    var rs = [];
    var rsi = [];

    for (var i = 0; i < candles.length; i++) {
        if (i - 1 < 0) {
            gain.push(0);
            loss.push(0);
        } else if (i - 1 >= 0) {
            var change = candles[i].close - candles[i - 1].close;
            if (change > 0) {
                gain.push(change);
                loss.push(0);
            } else if (change < 0) {
                gain.push(0);
                loss.push(-change);
            } else if (change === 0) {
                gain.push(0);
                loss.push(0);
            }
        }
    }


    //var alpha =  2 / (timeframe + 1);
    var alpha =  1 / (timeframe);
    for (var i = 0; i < gain.length; i++) {
        if (i < timeframe) {
            avgGain.push(0);
            avgLoss.push(0);
        } else if (i === timeframe) {
            var gainCalc = 0;
            var lossCalc = 0;
            for (var j = 0; j < timeframe; j++) {
                gainCalc = gainCalc + gain[i - j];
                lossCalc = lossCalc + loss[i - j];
            }
            avgGain.push(gainCalc / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir
            avgLoss.push(lossCalc / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir

        } else if (i > timeframe) {
            var avgGainCalc = gain[i]*alpha+(1-alpha)*avgGain[i-1];
            var avgLossCalc = loss[i]*alpha+(1-alpha)*avgLoss[i-1];
            avgGain.push(avgGainCalc);
            avgLoss.push(avgLossCalc);
        }
    }


    for (var i = 0; i < avgGain.length; i++) {
        var rsCalc = avgGain[i] === 0 ? 0 : avgGain[i] / avgLoss[i];
        rs.push(rsCalc);
    }

    for (var i = 0; i < rs.length; i++) {
        if (avgGain[i] === 0 && avgLoss[i] === 0) {
            rsi.push(0);
        } else if (avgGain[i] !== 0 && avgLoss[i] === 0) {
            rsi.push(100);
        } else if (avgGain[i] === 0 && avgLoss[i] !== 0) {
            rsi.push(0);
        } else {
            var calcRsi = 100 - (100 / (1 + rs[i]));
            rsi.push(calcRsi);
        }
    }
debugger;
    return rsi;
}

var candles=[];
var arra=[0.012952,0.012915,0.012977,0.012936,0.012838,0.01268200,0.012624,0.012455,0.012416,0.012487,0.012367,0.012551,0.01264400,0.012467,0.012535,0.012432,0.012391,0.012316];

arra.forEach((el)=>{
    candles.push({close:el});
});

var rsi = rsiCalculationEma(candles,14);

debugger;
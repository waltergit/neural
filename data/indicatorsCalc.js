var rsiCalculation = (candles, timeframe) => {

    //Método de Wilder
    var gain = [];
    var loss = [];
    var avgGain = [];
    var avgLoss = [];
    var rs = [];
    var rsi = [];

    for (var i = 0; i < candles.length; i++) {
        if (i - 1 < 0) {
            gain.push(0);
            loss.push(0);
        } else if (i - 1 >= 0) {
            var change = candles[i].close - candles[i - 1].close;
            if (change > 0) {
                gain.push(change);
                loss.push(0);
            } else if (change < 0) {
                gain.push(0);
                loss.push(-change);
            } else if (change === 0) {
                gain.push(0);
                loss.push(0);
            }
        }
    }

    for (var i = 0; i < gain.length; i++) {
        if (i < timeframe) {
            avgGain.push(0);
            avgLoss.push(0);
        } else if (i === timeframe) {
            var gainCalc = 0;
            var lossCalc = 0;
            for (var j = 0; j < timeframe; j++) {
                gainCalc = gainCalc + gain[i - j];
                lossCalc = lossCalc + loss[i - j];
            }
            avgGain.push(gainCalc / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir
            avgLoss.push(lossCalc / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir

        } else if (i > timeframe) {
            var avgGainCalc = (avgGain[i - 1] * (timeframe - 1) + gain[i]) / timeframe;
            var avgLossCalc = (avgLoss[i - 1] * (timeframe - 1) + loss[i]) / timeframe;
            avgGain.push(avgGainCalc);
            avgLoss.push(avgLossCalc);
        }
    }

    for (var i = 0; i < avgGain.length; i++) {
        var rsCalc = avgGain[i] === 0 ? 0 : avgGain[i] / avgLoss[i];
        rs.push(rsCalc);
    }

    for (var i = 0; i < rs.length; i++) {
        if (avgGain[i] === 0 && avgLoss[i] === 0) {
            rsi.push(0);
        } else if (avgGain[i] !== 0 && avgLoss[i] === 0) {
            rsi.push(100);
        } else if (avgGain[i] === 0 && avgLoss[i] !== 0) {
            rsi.push(0);
        } else {
            var calcRsi = 100 - (100 / (1 + rs[i]));
            rsi.push(calcRsi);
        }
    }
    
    return rsi;
}

var rsiCalculationEma = (candles, timeframe) => {

    //Método de Wilder
    var gain = [];
    var loss = [];
    var avgGain = [];
    var avgLoss = [];
    var rs = [];
    var rsi = [];

    for (var i = 0; i < candles.length; i++) {
        if (i - 1 < 0) {
            gain.push(0);
            loss.push(0);
        } else if (i - 1 >= 0) {
            var change = candles[i].close - candles[i - 1].close;
            if (change > 0) {
                gain.push(change);
                loss.push(0);
            } else if (change < 0) {
                gain.push(0);
                loss.push(-change);
            } else if (change === 0) {
                gain.push(0);
                loss.push(0);
            }
        }
    }


    var alpha = 1 / (timeframe + 1);
    //var alpha =  1 / (timeframe);
    for (var i = 0; i < gain.length; i++) {
        if (i < timeframe) {
            avgGain.push(0);
            avgLoss.push(0);
        } else if (i === timeframe) {
            var gainCalc = 0;
            var lossCalc = 0;
            for (var j = 0; j < timeframe; j++) {
                gainCalc = gainCalc + gain[i - j];
                lossCalc = lossCalc + loss[i - j];
            }
            avgGain.push(gainCalc / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir
            avgLoss.push(lossCalc / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir

        } else if (i > timeframe) {
            var avgGainCalc = gain[i] * alpha + (1 - alpha) * avgGain[i - 1];
            var avgLossCalc = loss[i] * alpha + (1 - alpha) * avgLoss[i - 1];
            avgGain.push(avgGainCalc);
            avgLoss.push(avgLossCalc);
        }
    }


    for (var i = 0; i < avgGain.length; i++) {
        var rsCalc = avgGain[i] === 0 ? 0 : avgGain[i] / avgLoss[i];
        rs.push(rsCalc);
    }

    for (var i = 0; i < rs.length; i++) {
        if (avgGain[i] === 0 && avgLoss[i] === 0) {
            rsi.push(0);
        } else if (avgGain[i] !== 0 && avgLoss[i] === 0) {
            rsi.push(100);
        } else if (avgGain[i] === 0 && avgLoss[i] !== 0) {
            rsi.push(0);
        } else {
            var calcRsi = 100 - (100 / (1 + rs[i]));
            rsi.push(calcRsi);
        }
    }

    return rsi;
}

var rsiCalculationCutler = (candles, timeframe) => {

    //Método de Cutler
    var gain = [];
    var loss = [];
    var avgGain = [];
    var avgLoss = [];
    var rs = [];
    var rsi = [];

    for (var i = 0; i < candles.length; i++) {
        if (i - 1 < 0) {
            gain.push(0);
            loss.push(0);
        } else if (i - 1 >= 0) {
            var change = candles[i].close - candles[i - 1].close;
            if (change > 0) {
                gain.push(change);
                loss.push(0);
            } else if (change < 0) {
                gain.push(0);
                loss.push(-change);
            } else if (change === 0) {
                gain.push(0);
                loss.push(0);
            }
        }
    }

    avgGain = smaCalculation(gain, timeframe);
    avgLoss = smaCalculation(loss, timeframe);

    for (var i = 0; i < avgGain.length; i++) {
        var rsCalc = avgGain[i] === 0 ? 0 : avgGain[i] / avgLoss[i];
        rs.push(rsCalc);
    }

    for (var i = 0; i < rs.length; i++) {
        if (avgGain[i] === 0 && avgLoss[i] === 0) {
            rsi.push(0);
        } else if (avgGain[i] !== 0 && avgLoss[i] === 0) {
            rsi.push(100);
        } else if (avgGain[i] === 0 && avgLoss[i] !== 0) {
            rsi.push(0);
        } else {
            var calcRsi = 100 - (100 / (1 + rs[i]));
            rsi.push(calcRsi);
        }
    }

    return rsi;
}

var stochRsiCalculation = (rsi, timeframe) => {

    var stochRsi = [];
    var lowestLow = 0;
    var highestHigh = 0;

    for (var i = 0; i < rsi.length; i++) {
        lowestLow = rsi[i];
        highestHigh = rsi[i];
        for (var j = 1; j < timeframe; j++) {
            if (i - j >= 0) {
                if (rsi[i - j] > highestHigh) {
                    highestHigh = rsi[i - j];
                }
                if (rsi[i - j] < lowestLow) {
                    lowestLow = rsi[i - j];
                }
            }
        }

        var stochCalc = (rsi[i] - lowestLow) === 0 ? 0 : (rsi[i] - lowestLow) / (highestHigh - lowestLow);
        stochRsi.push(stochCalc);
    }

    return stochRsi;
}

var smaCalculationNew = (input, timeframe) => {
    var output = [];

    for (var i = 0; i < input.length; i++) {
        if (i < (timeframe - 1)) {
            output.push(0);
        } else {

            var suma = 0;
            for (var j = 0; j < timeframe; j++) {

                suma = suma + input[i - j];

            }
            var sma = suma / timeframe;

            output.push(sma);
        }


    }
    return output;
}

var smaCalculation = (input, timeframe) => {
    var output = [];
    for (var i = 0; i < input.length; i++) {
        var suma = input[i];
        for (var j = 1; j < timeframe; j++) {
            if (i - j >= 0) {
                suma = suma + input[i - j];
            }
        }
        var sma = suma / timeframe;

        output.push(sma);
    }

    return output;
}

var emaCalculation = (input, timeframe) => {
    var output = [];
    var smoothingConstant = 2 / (timeframe + 1);
    var sma = smaCalculation(input, timeframe);

    for (var i = 0; i < sma.length; i++) {
        if (i < (timeframe - 1)) {
            output.push(0);
        } else if (i === (timeframe - 1)) {
            output.push(sma[i]);
        } else if (i > (timeframe - 1)) {

            var calc = smoothingConstant * (input[i] - output[i - 1]) + output[i - 1];
            output.push(calc);
        }
    }

    return output;
}

var mmaTa4j = (input, timeframe) => {
    var multiplier = 1 / timeframe;
    var output = [];
    for (var i = 0; i < input.length; i++) {
        if (i === 0) {
            output.push(input[i]);
        } else {
            let prevValue = input[i - 1];
            let calc = (input[i] - prevValue) * multiplier + prevValue;
            output.push(calc);
        }
    }
    return output;
}

var mmaTradingView = (input, timeframe) => {

    var output = [];
    for (var i = 0; i < input.length; i++) {
        if (i < timeframe) {
            output.push(0);

        } else if (i === timeframe) {
            var sum = 0;

            for (var j = 0; j < timeframe; j++) {
                sum = sum + input[i - j];

            }
            output.push(sum / timeframe);  //Probar dividiendo aquí por timeframe: duda de si en tradingview se equivocaban al no dividir


        } else if (i > timeframe) {
            var calc = (output[i - 1] * (timeframe - 1) + input[i]) / timeframe;

            output.push(calc);

        }
    }
    return output;
}

var emaTa4j = (input, timeframe) => {
    var multiplier = 2 / (timeframe + 1);
    var output = [];
    for (var i = 0; i < input.length; i++) {
        if (i === 0) {
            output.push(input[i]);
        } else {
            var prevValue = input[i - 1];
            var calc = (input[i] - prevValue) * multiplier + prevValue;
            output.push(calc);
        }
    }
    return output;
}


var smreddit = (input, timeframe) => {
    var multiplier = 1 / timeframe;
    var output = [];
    var sum = 0;
    for (var i = 0; i < input.length; i++) {
        if (i < timeframe) {
            output.push(0);

        } else if (i === timeframe) {

            for (var j = 0; j <= i; j++) {
                sum = sum + input[j];
            }
            output.push(sum / timeframe);

        } else if (i > timeframe) {

            var prevValue = output[i - 1];
            var calc = input[i] * multiplier + (1 - multiplier) * prevValue;
            output.push(calc);
        }


    }
    return output;
}

var smaTa4j = (input, timeframe) => {
    var output = [];
    for (var i = 0; i < input.length; i++) {
        var sum = 0;
        for (var j = Math.max(0, i - timeframe + 1); j <= i; j++) {
            sum = sum + input[j];
        }
        var realTimeFrame = Math.min(timeframe, i + 1);
        var calc = sum / (realTimeFrame);
        output.push(calc);
    }
    return output;
}

var gainTa4j = (candles) => {
    var output = [];
    for (var i = 0; i < candles.length; i++) {
        if (i === 0) {
            output.push(0);
        } else {

            if (candles[i].close > candles[i - 1].close) {
                output.push(candles[i].close - candles[i - 1].close);
            } else {
                output.push(0);
            }
        }

    }
    return output;
}

var lossTa4j = (candles) => {
    var output = [];
    for (var i = 0; i < candles.length; i++) {
        if (i === 0) {
            output.push(0);
        } else {

            if (candles[i].close < candles[i - 1].close) {
                output.push(candles[i - 1].close - candles[i].close);
            } else {
                output.push(0);
            }
        }

    }
    return output;
}

var rsiTa4j = (candles, timeframe) => {
    var averageGainIndicator = smreddit(gainTa4j(candles), timeframe);
    var averageLossIndicator = smreddit(lossTa4j(candles), timeframe);
    var output = [];
    for (var i = 0; i < candles.length; i++) {
        var averageGain = averageGainIndicator[i];
        var averageLoss = averageLossIndicator[i];
        if (averageLoss === 0) {
            if (averageGain === 0) {
                output.push(0);
            } else {
                output.push(100);
            }
        } else {
            var relativeStrength = averageGain / averageLoss;
            output.push(100 - (100 / (1 + relativeStrength)));
        }

    }
    return output;
}

var lowestValue = (input, timeframe) => {
    var output = [];
    for (var i = 0; i < input.length; i++) {
        var end = Math.max(0, i - timeframe + 1);
        var lowest = input[i];
        for (var j = i - 1; j >= end; j--) {
            if (lowest > input[j]) {
                lowest = input[j]
            }
        }
        output.push(lowest);
    }
    return output;
}

var highestValue = (input, timeframe) => {
    var output = [];
    for (var i = 0; i < input.length; i++) {
        var end = Math.max(0, i - timeframe + 1);
        var highest = input[i];
        for (var j = i - 1; j >= end; j--) {
            if (highest < input[j]) {
                highest = input[j]
            }
        }
        output.push(highest);
    }
    return output;
}

var stochRsiTa4j = (rsi, timeframe) => {

    var minRsi = lowestValue(rsi, timeframe);
    var maxRsi = highestValue(rsi, timeframe);
    var output = [];

    for (var i = 0; i < rsi.length; i++) {
        var minRsiValue = minRsi[i];
        output.push((rsi[i] - minRsiValue) / (maxRsi[i] - minRsiValue));
    }

    return output;
}

var standardDeviationCalc = (closePrices, timeframe) => {
    var output = [];
    var deviation = [];
    var deviationSquared = [];
    var sma = smaTa4j(closePrices, timeframe);

    for (var i = 0; i < closePrices.length; i++) {
        deviation = [];
        for (var j = 0; j < timeframe; j++) {
            if (i - j >= 0) {
                deviation.push(closePrices[i - j] - sma[i]);
            }

        }
        deviationSquared = [];
        for (var j = 0; j < deviation.length; j++) {
            deviationSquared.push(Math.pow(deviation[j], 2));
        }
        var sum = 0;
        for (var j = 0; j < deviationSquared.length; j++) {
            sum = sum + deviationSquared[j];
        }
        output.push(Math.sqrt(sum / timeframe));

    }
    return output;
}

var bollingerUpCalc = (closePrices, timeframe, volume) => {
    var output = [];
    var standardDeviation = standardDeviationCalc(closePrices, timeframe);
    var bollinguerMid = smaTa4j(closePrices, timeframe);
    for (var i = 0; i < bollinguerMid.length; i++) {
        output.push(bollinguerMid[i] + (standardDeviation[i] * volume));
    }
    return output;
}

var bollingerLowCalc = (closePrices, timeframe, volume) => {
    var output = [];
    var standardDeviation = standardDeviationCalc(closePrices, timeframe);
    var bollinguerMid = smaTa4j(closePrices, timeframe);
    for (var i = 0; i < bollinguerMid.length; i++) {
        output.push(bollinguerMid[i] - (standardDeviation[i] * volume));
    }
    return output;
}

module.exports = {
    rsiCalculation,
    rsiCalculationEma,
    rsiCalculationCutler,
    stochRsiCalculation,
    smaCalculationNew,
    smaCalculation,
    emaCalculation,
    mmaTa4j,
    mmaTradingView,
    emaTa4j,
    smreddit,
    smaTa4j,
    gainTa4j,
    lossTa4j,
    rsiTa4j,
    lowestValue,
    highestValue,
    stochRsiTa4j,
    standardDeviationCalc,
    bollingerUpCalc,
    bollingerLowCalc
}
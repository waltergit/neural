var bddService = require('./bddService.js');
const api = require('binance');
const ind = require('./indicatorsCalc.js');

const binanceRest = new api.BinanceRest({
    key: 'nQBLUUBgH6yG4Lx44UAxLOwIFgDCv30cfrL6WyuMnXqlO2wqKzqHAGNYkuYrkdxA', // Get this from your account on binance.com
    secret: '2HLzU0abZdVRXy555gfeMq24LNrDvyJDa8WoDrp0iT8dJ4oPuvOwtEqxB4uBWRZF', // Same for this
    timeout: 15000, // Optional, defaults to 15000, is the request time out in milliseconds
    recvWindow: 10000, // Optional, defaults to 5000, increase if you're getting timestamp errors
    disableBeautification: false,
    /*
     * Optional, default is false. Binance's API returns objects with lots of one letter keys.  By
     * default those keys will be replaced with more descriptive, longer ones.
     */
    handleDrift: false
    /* Optional, default is false.  If turned on, the library will attempt to handle any drift of
     * your clock on it's own.  If a request fails due to drift, it'll attempt a fix by requesting
     * binance's server time, calculating the difference with your own clock, and then reattempting
     * the request.
     */
});

const binanceWS = new api.BinanceWS(true); // Argument specifies whether the responses should be beautified, defaults to true

var getHistoricalTrades = (symbol, limit, fromId) => {

    var options = {
        symbol: symbol.toUpperCase(),
        limit: limit,
        fromId: fromId
    }
    for (var property in options) {
        if (options.hasOwnProperty(property)) {
            if (options[property] === undefined) {
                delete options[property];
            }
        }
    }
    return binanceRest.historicalTrades(options);

}

var bddSentencer = (arrayToInsert, tableName, stringOrderedObjectAttributes) => {
    let sentence = `INSERT INTO ${tableName} VALUES `;
    arrayToInsert.forEach((element) => {
        var object = `(`;
        stringOrderedObjectAttributes.forEach((el) => {
            object = object.concat(`${element[el]},`);
        });
        object = object.slice(0, -1);
        object = object.concat(`),`)
        sentence = sentence.concat(object);
    });
    return sentence = sentence.slice(0, -1);
}

bddService.startWriting();

/* 

let update = {
    sentence: `UPDATE MARKET_FOUNDS
    SET EUR = 12
    WHERE MARKET = DSX`
    //args: [money, 'BIBOX']
}

bddService.pushQueueRun(update); */



/* binance.websockets.trades(['TRXETH'], (trades) => {
    let { e: eventType, E: eventTime, s: symbol, p: price, q: quantity, m: maker, a: tradeId } = trades;
    
    console.log(symbol + " trade update. price: " + price + ", quantity: " + quantity + ", maker: " + maker);
}); */

/* var getAggregateTrades('trxeth',undefined,undefined,undefined,500).then(r => {
    
    
    let sentencia = bddSentencer(r, 'AGGREGATE_TRADES', ['a', 'p', 'q', 'T']);
    let update = {
        sentence: sentencia,
        args: []
    }
    bddService.pushQueueRun(update);

}) */



/* async function f() {

    try {
        var trades = await getAggregateTrades('trxeth', undefined, undefined, undefined, 500);
    } catch (e) {

    }
    var candles = [];
    trades.forEach((trade)=>{
        trade.time=new Date (trade.T);
    });
    
}

f(); */


/* getHistoricalTrades('trxeth', 500, undefined).then(r => {
    
    console.log("Hola");
}) */

var historicalTradesToCandles = (historicalTrades) => {
    var arrOfHistoricalTradesByMin = [];
    var arrPerMin = [];
    var candles = [];

    for (var i = 0; i < historicalTrades.length; i++) {


        historicalTrades[i].dateDate = new Date(historicalTrades[i].time);
        if (historicalTrades.indexOf(historicalTrades[i]) === 0) {
            arrPerMin.push(historicalTrades[i]);
        } else if (arrPerMin.length > 0 && sameMinute(arrPerMin[arrPerMin.length - 1], historicalTrades[i])) {
            arrPerMin.push(historicalTrades[i]);
        } else if (arrPerMin.length > 0 && !sameMinute(arrPerMin[arrPerMin.length - 1], historicalTrades[i]) && oneMinDif(arrPerMin[arrPerMin.length - 1], historicalTrades[i])) {
            arrOfHistoricalTradesByMin.push(arrPerMin);
            arrPerMin = [];
            arrPerMin.push(historicalTrades[i]);
        } else if (!sameMinute((arrPerMin.length > 0 ? arrPerMin[arrPerMin.length - 1] : arrOfHistoricalTradesByMin[arrOfHistoricalTradesByMin.length - 1][arrOfHistoricalTradesByMin[arrOfHistoricalTradesByMin.length - 1].length - 1]), historicalTrades[i]) && !oneMinDif((arrPerMin.length > 0 ? arrPerMin[arrPerMin.length - 1] : arrOfHistoricalTradesByMin[arrOfHistoricalTradesByMin.length - 1][arrOfHistoricalTradesByMin[arrOfHistoricalTradesByMin.length - 1].length - 1]), historicalTrades[i])) {

            if (arrPerMin.length > 0) {
                arrOfHistoricalTradesByMin.push(arrPerMin);
                arrPerMin = [];
            }

            let lastTrade = clone(arrOfHistoricalTradesByMin[arrOfHistoricalTradesByMin.length - 1][arrOfHistoricalTradesByMin[arrOfHistoricalTradesByMin.length - 1].length - 1]);
            let fakeTrade = {
                dateDate: clone(lastTrade.dateDate),
                id: 0,
                price: clone(lastTrade.price),
                qty: 0,
                time: clone(lastTrade.time)
            }
            fakeTrade.dateDate.setMinutes(fakeTrade.dateDate.getMinutes() + 1);
            fakeTrade.dateDate.setMilliseconds(0);
            fakeTrade.dateDate.setSeconds(0);
            fakeTrade.time = fakeTrade.dateDate.getTime();


            arrPerMin.push(fakeTrade);
            arrOfHistoricalTradesByMin.push(arrPerMin);
            arrPerMin = [];

            i--;


        } else {
            arrPerMin.push(historicalTrades[i]);
        }

        if (arrPerMin.length > 0 && historicalTrades.indexOf(historicalTrades[i]) === (historicalTrades.length - 1)) {

            arrOfHistoricalTradesByMin.push(arrPerMin);
            arrPerMin = [];

        }
    }

    arrOfHistoricalTradesByMin.forEach((tradesByMin) => {
        var open = Number(tradesByMin[0].price);
        var close = Number(tradesByMin[tradesByMin.length - 1].price);
        var volume = 0;
        var high = Math.max.apply(Math, tradesByMin.map(function (o) { return Number(o.price); }));
        var low = Math.min.apply(Math, tradesByMin.map(function (o) { return Number(o.price); }));;
        tradesByMin.forEach((trade) => {
            volume = volume + Number(trade.qty);
        })
        var date = new Date(tradesByMin[0].dateDate.setMilliseconds(0)).setSeconds(0);
        var dateDate = new Date(date);
        var candle = {
            open, close, volume, high, low, date, dateDate, trades: tradesByMin
        }
        candles.push(candle);
    })

    return candles;
}

var sameMinute = (tradeReference, tradeToCompare) => {
    var cond1 = tradeReference.dateDate.getDate() === tradeToCompare.dateDate.getDate();
    var cond2 = tradeReference.dateDate.getHours() === tradeToCompare.dateDate.getHours();
    var cond3 = tradeReference.dateDate.getMinutes() === tradeToCompare.dateDate.getMinutes();
    return (cond1 && cond2 && cond3);
}

var oneMinDif = (tradeReference, tradeToCompare) => {
    var reference = clone(tradeReference.dateDate);
    reference.setMinutes(reference.getMinutes() + 1);
    var cond1 = reference.getDate() === tradeToCompare.dateDate.getDate();
    var cond2 = reference.getHours() === tradeToCompare.dateDate.getHours();
    var cond3 = reference.getMinutes() === tradeToCompare.dateDate.getMinutes();
    return (cond1 && cond2 && cond3);
}


var getClosePrices = (candles) => {
    var output = [];
    candles.forEach((el) => {
        output.push(el.close);
    });
    return output;
}



//Método para obtener pares de compraVenta ideales mediante candles y bollinguerMid 

var getParesCompraVenta = (candles, indicadores) => {

    var tradesDebajoBollMid = [];
    var tradesEncimaBollMid = [];
    var paresIdeales = [];


    for (var i = 0; i < candles.length; i++) {

        var candle = candles[i];
        //Precio más caro y precio más barato de la candle
        //var maxRes = Math.max.apply(Math, candle.trades.map(function (o) { return o.price; }));
        //var minRes = Math.min.apply(Math, candle.trades.map(function (o) { return o.price; }));
        //Lo hago mejor con los open close para eliminar el ruido de los picos high low --> Igual habría que probar considerándo estos como maxRes y minRes como en comentado
        var maxRes = Math.max(candle.open, candle.close);
        var minRes = Math.min(candle.open, candle.close);

        //Recupero para la candle el trade más caro y el más barato
        var maxTrade = candle.trades.find(function (o) { return o.price == maxRes; });
        var minTrade = candle.trades.find(function (o) { return o.price == minRes; });


        //Aqui selecciono los trades de compra y venta interesantes
        //Luego me calculo los indicadores TRADE a TRADE porque quiero que el bot actue en VIVO
        //Outputs con indicadores EN VIVO

        if (i <= candles.length - 2 && candles[i].close < indicadores.bollinguerMid[i] && candles[i + 1].close < indicadores.bollinguerMid[i + 1] && tradesEncimaBollMid.length === 0) {
            tradesDebajoBollMid.push(minTrade);
        } else if (i <= candles.length - 2 && tradesDebajoBollMid.length > 0 && candles[i].close > indicadores.bollinguerMid[i] && candles[i + 1].close > indicadores.bollinguerMid[i + 1]) {
            tradesEncimaBollMid.push(maxTrade);
        }

        //Cuando ya ha pasado el bollMid definitivamente hacia abajo mando pares ideales y reseteo arrays de recogida
        if (i <= candles.length - 2 && tradesDebajoBollMid.length > 0 && tradesEncimaBollMid.length > 0 && candles[i].close < indicadores.bollinguerMid[i] && candles[i + 1].close < indicadores.bollinguerMid[i + 1]) {

            paresIdeales.push(getParTrades(tradesDebajoBollMid, tradesEncimaBollMid));
            tradesDebajoBollMid = [];
            tradesEncimaBollMid = [];
            tradesDebajoBollMid.push(minTrade);

        }


        //Al final si hay algo en los 2 arrays lo paso también.
        if (i === candles.length - 1 && tradesDebajoBollMid.length > 0 && tradesEncimaBollMid.length > 0) {

            paresIdeales.push(getParTrades(tradesDebajoBollMid, tradesEncimaBollMid));
            tradesDebajoBollMid = [];
            tradesEncimaBollMid = [];
            debugger;
        }


    }

    return paresIdeales;

}

//Función a la que le paso un monton de trades por debajo de boll mid y por encima de boll mid y me devuelve el par de trades ideales de compra/venta

var getParTrades = (tradesDebajoBollMid, tradesEncimaBollMid) => {

    var minTradePrice = Math.min.apply(Math, tradesDebajoBollMid.map(function (o) { return o.price; }));
    var maxTradePrice = Math.max.apply(Math, tradesEncimaBollMid.map(function (o) { return o.price; }));

    var compraIdeal = tradesDebajoBollMid.find(function (o) { return o.price == minTradePrice; });
    var ventaIdeal = tradesEncimaBollMid.find(function (o) { return o.price == maxTradePrice; });

    return { compraIdeal, ventaIdeal };

}



var mediaArrays = (arr1, arr2) => {
    var output = [];
    for (var i = 0; i < arr1.length; i++) {
        output.push((arr1[i] + arr2[i]) / 2);
    }
    return output;
}

var getCandles = async (symbol, minCandles) => {
    var historicalTrades = await getHistoricalTrades(symbol, 500, undefined);
    var candles = historicalTradesToCandles(historicalTrades);
    for (var i = 0; ; i++) {
        if (candles.length >= minCandles) {
            break;
        } else {
            historicalTrades = pushArray(await getHistoricalTrades(symbol, 500, historicalTrades[0].id - 500), historicalTrades);
            candles = historicalTradesToCandles(historicalTrades);

        }
    }
    return candles;
}

var pushArray = (principio, final) => {

    return principio.concat(final);
}

function clone(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

var funk = async () => {

    /*  var historicalTrades = await getHistoricalTrades('trxeth', 10, undefined);
 
     
     historicalTrades = pushArray(await getHistoricalTrades('trxeth', 10, historicalTrades[0].id - 10), historicalTrades);
     
     var candles = historicalTradesToCandles(historicalTrades); */

    var candles = await getCandles('bnbeth', 200);
    //Rsi de trading view
    var rsiTradingView = ind.rsiCalculation(candles, 14);
    //Rsi de cutler
    var rsiBinance = ind.rsiCalculationEma(candles, 14);

    //Stoch Rsi
    var stochRsiTradingView = ind.stochRsiCalculation(rsiTradingView, 9);
    var stochRsiBinance = ind.stochRsiCalculation(rsiBinance, 9);

    //K y D de binance
    var k = ind.emaCalculation(stochRsiBinance, 3);
    var d = ind.emaCalculation(k, 3);

    //K y D de trading view
    var kSma = ind.smaCalculation(stochRsiTradingView, 3);
    var dSma = ind.smaCalculation(kSma, 3);


    var rsi = ind.rsiTa4j(candles, 14);
    var sRsi = ind.stochRsiTa4j(rsi, 9);
    var _k = ind.smaTa4j(sRsi, 3);
    var _d = ind.smaTa4j(_k, 3);

    var closePrices = getClosePrices(candles);
    var ema7 = ind.emaTa4j(closePrices, 7);
    var ema25 = ind.emaTa4j(closePrices, 25);
    var ema99 = ind.emaTa4j(closePrices, 99);

    //El más aproximado a trading view.
    var _ema7 = ind.emaCalculation(closePrices, 7);
    var _ema25 = ind.emaCalculation(closePrices, 25);
    var _ema99 = ind.emaCalculation(closePrices, 99);


    //Identicos
    var smaTaj = ind.smaTa4j(closePrices, 21);
    var smaCalc = ind.smaCalculation(closePrices, 21);

    var bollingerMid = ind.smaTa4j(closePrices, 21);
    var bollingerUp = ind.bollingerUpCalc(closePrices, 21, 2);
    var bollingerLow = ind.bollingerLowCalc(closePrices, 21, 2);

    var indicadores = {
        rsi: rsi,
        bollinguerMid: bollingerMid,
        bollingerLow: bollingerLow,
        bollingerUp: bollingerUp
    }

    var paresIdeales = getParesCompraVenta(candles, indicadores);

    debugger;
    console.log(rsi);
}


//Aquí abrimos el webSocket para ir recogiendo los trades

/* binanceWS.onTrade('TRXETH', (data) => {
    debugger;
    console.log(data);
}); */

funk();




const sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database('./bdd/tablas.db', (err) => {
    if (err) {
        return console.error(err.message);
    }

});

// A simple async series:
var items = [];


var series = (item) => {
    
    if (item) {
        
        db.run(item.sentence, item.args, () => {
            return series(items.shift());
        });
    } else {
        
        setTimeout(()=>{
            return series(items.shift());
        },300);

    }
}

module.exports.startWriting = () => {
    series(items.shift());
}

module.exports.pushQueueRun = (item) => {
    items.push(item);
}

module.exports.read = (sentence, callback) => {
    db.all(sentence, [], (err, rows) => {
        if (err) {
            throw err;
        }
        callback(rows);
    });
}


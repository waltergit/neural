var options = {
    fromId: 1,
    startTime: 2,
    endTime: 3,
    limit: 4
}

for (var property in options) {
    if (options.hasOwnProperty(property)) {
        if (options[property] === 4) {
            delete options[property];
        }
    }
}

console.log(options);